#include <ctime>
#include <windows.h.>
#include <gl\gl.h>

class Timer 
{
public:
  Timer() : start (std::clock()) { }
public:
  void begin()
  {
	  start = std::clock();
  }

  double end()
  {
	  const GLfloat elapsed = static_cast<GLfloat>(std::clock()) - start;
	  return elapsed / CLOCKS_PER_SEC;
  }

private:
	std::clock_t start;
};