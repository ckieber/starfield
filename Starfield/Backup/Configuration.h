#ifndef __Configuration__
#define __Configuration__

#include <windows.h.>
#include <gl\gl.h>

class Configuration
{
public:
	Configuration(const int numStars,
				  const GLfloat radius,
				  const GLfloat rotation,
				  const GLfloat speed,
				  const GLfloat time);
	~Configuration();

	//set methods
	void setNumStars(const int numStars);
	void setRadius(const GLfloat radius);
	void setRotation(const GLfloat rotation);
	void setSpeed(const GLfloat speed);
	void setTime(const GLfloat time);

	//get methods
	int getNumStars() const;
	GLfloat getRadius() const;
	GLfloat getRotation() const;
	GLfloat getSpeed() const;
	GLfloat getTime() const;

private:
	int mNumStars;
	GLfloat mRadius;
	GLfloat mRotation;
	GLfloat mSpeed;
	GLfloat mTime;
};

#endif