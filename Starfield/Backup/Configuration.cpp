#include "Configuration.h"

Configuration::Configuration(const int numStars,
							 const GLfloat radius,
							 const GLfloat rotation,
							 const GLfloat speed,
							 const GLfloat time)
{
	mNumStars = numStars;
	mRadius = radius;
	mRotation = rotation;
	mSpeed = speed;
	mTime = time;
}

Configuration::~Configuration()
{

}

int Configuration::getNumStars() const
{
	return mNumStars;
}

GLfloat Configuration::getRadius() const
{
	return mRadius;
}

GLfloat Configuration::getRotation() const
{
	return mRotation;
}

GLfloat Configuration::getSpeed() const
{
	return mSpeed;
}

GLfloat Configuration::getTime() const
{
	return mTime;
}

void Configuration::setNumStars(const int numStars)
{
	mNumStars = numStars;
}

void Configuration::setRadius(const GLfloat radius)
{
	mRadius = radius;
}

void Configuration::setRotation(const GLfloat rotation)
{
	mRotation = rotation;
}

void Configuration::setSpeed(const GLfloat speed)
{
	mSpeed = speed;
}

void Configuration::setTime(const GLfloat time)
{
	mTime = time;
}