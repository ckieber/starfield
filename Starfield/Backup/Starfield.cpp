#include <windows.h>							// Header File For Windows
#include <stdio.h>								// Header File For Standard Input/Output
#include <cmath>
#include <time.h>
#include <gl\gl.h>								// Header File For The OpenGL32 Library
#include <gl\glu.h>								// Header File For The GLu32 Library
#include "BMP.h"								// Header File For The BMP File (GLaux Library Replacement)
#include "Configuration.h"

#define MAX_STARS 300

#define MAX_RADIUS 10.0
#define MIN_RADIUS 0.5
#define RADIUS_INC 0.5

#define MAX_ROT 100.0
#define MIN_ROT -100.0
#define ROT_INC 10.0

#define MAX_SPEED 40.0
#define MIN_SPEED 0.0
#define DEFAULT_SPEED 16.0
#define SPEED_INC 2.0

//game loop variables
#define LOGIC_FPS 60.0
LARGE_INTEGER performanceFrequency, oldTime, newTime;
GLfloat logicFpsStep = (GLfloat)(1.0 / LOGIC_FPS), frameDelta = 0, logicUpdateCounter = 0;

HGLRC hRC = NULL;								// Permanent Rendering Context
HDC hDC = NULL;									// Private GDI Device Context
HWND hWnd = NULL;								// Holds Our Window Handle
HINSTANCE hInstance;							// Holds The Instance Of The Application

bool keys[256];									// Array Used For The Keyboard Routine
bool active = true;								// Window Active Flag Set To TRUE By Default
bool fullscreen = true;							// Fullscreen Flag Set To Fullscreen Mode By Default

bool showColors = true;
bool vkCPressed = false;

bool vkUPPressed = false;
bool vkDownPressed = false;

bool vkRightPressed = false;
bool vkLeftPressed = false;

bool vkPriorPressed = false;
bool vkNextPressed = false;

int num = MAX_STARS;							// Number Of Stars To Draw
GLfloat startPos = -50;							// Viewing Distance Away From Stars
GLfloat radius = 3.0f;
GLfloat speed = DEFAULT_SPEED;
GLfloat angle = 0.0f;
GLfloat angleSpeed = 0.0f;

typedef struct
{
	GLfloat x, y, z;
}
vec3;

typedef struct									// Create A Structure For Star
{
	int r, g, b;								// Stars Color
	vec3 position;								// Stars Position
}
stars;											// Structures Name Is Stars
stars star[MAX_STARS];								// Make 'star' Array Of 'num' Using Info From The Structure 'stars'

GLuint count;									// General Loop Variable
GLuint texture[1];								// Storage For One Texture

LRESULT	CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);	// Declaration For WndProc

AUX_RGBImageRec *LoadBMP(char *Filename)		// Loads A Bitmap Image
{
	FILE *File = NULL;							// File Handle

	if (!Filename)								// Make Sure A Filename Was Given
	{
		return NULL;							// If Not Return NULL
	}

	File = fopen(Filename,"r");					// Check To See If The File Exists

	if (File)									// Does The File Exist?
	{
		fclose(File);							// Close The Handle
		return auxDIBImageLoad(Filename);		// Load The Bitmap And Return A Pointer
	}
	return NULL;								// If Load Failed Return NULL
}

bool LoadGLTextures()							// Load Bitmaps And Convert To Textures
{
	bool Status = false;						// Status Indicator

	AUX_RGBImageRec *TextureImage[1];			// Create Storage Space For The Texture

	memset(TextureImage,0,sizeof(void *)*1);	// Set The Pointer To NULL

	// Load The Bitmap, Check For Errors, If Bitmap's Not Found Quit
	if (TextureImage[0] = LoadBMP("../Data/Star.bmp"))
	{
		Status = true;							// Set The Status To TRUE

		glGenTextures(1, &texture[0]);			// Create One Texture

		// Create Linear Filtered Texture
		glBindTexture(GL_TEXTURE_2D, texture[0]);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, TextureImage[0]->sizeX, TextureImage[0]->sizeY, 0, GL_RGB, GL_UNSIGNED_BYTE, TextureImage[0]->data);
	}

	if (TextureImage[0])						// If Texture Exists
	{
		if (TextureImage[0]->data)				// If Texture Image Exists
		{
			free(TextureImage[0]->data);		// Free The Texture Image Memory
		}

		free(TextureImage[0]);					// Free The Image Structure
	}

	return Status;								// Return The Status
}



GLvoid ReSizeGLScene(GLsizei width, GLsizei height)		// Resize And Initialize The GL Window
{
	if (height == 0)							// Prevent A Divide By Zero By
	{
		height = 1;								// Making Height Equal One
	}

	glViewport(0, 0, width, height);			// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);				// Select The Projection Matrix
	glLoadIdentity();							// Reset The Projection Matrix

	// Calculate The Aspect Ratio Of The Window
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	glMatrixMode(GL_MODELVIEW);					// Select The Modelview Matrix
	glLoadIdentity();							// Reset The Modelview Matrix
}

GLfloat randFloatRange(GLfloat a, GLfloat b)
{
	return ((b-a) * ((GLfloat)rand() / RAND_MAX)) + a;
}

void createStars(bool inverse)
{
	if(!inverse)
	{
		star[count].position.x = randFloatRange(-radius, radius);
		int plusMinus = rand() % 2;
		if(plusMinus == 0)
			star[count].position.y = sqrt((GLfloat)pow(radius, 2) - pow(star[count].position.x, 2));
		else
			star[count].position.y = -sqrt((GLfloat)pow(radius, 2) - pow(star[count].position.x, 2));
	}
	else
	{
		star[count].position.y = randFloatRange(-radius, radius);
		int plusMinus = rand() % 2;
		if(plusMinus == 0)
			star[count].position.x = sqrt((GLfloat)pow(radius, 2) - pow(star[count].position.y, 2));
		else
			star[count].position.x = -sqrt((GLfloat)pow(radius, 2) - pow(star[count].position.y, 2));
	}

	star[count].position.z = randFloatRange(startPos / 2, startPos);

	if(showColors)
	{
		star[count].r = rand() % 256;				// Give star[count] A Random Red Intensity
		star[count].g = rand() % 256;				// Give star[count] A Random Green Intensity
		star[count].b = rand() % 256;				// Give star[count] A Random Blue Intensity
	}
	else
	{
		star[count].r = 255;
		star[count].g = 255;
		star[count].b = 255;
	}
}

int InitGL(GLvoid)								// All Setup For OpenGL Goes Here
{
	if (!LoadGLTextures())						// Jump To Texture Loading Routine
	{
		return false;							// If Texture Didn't Load Return FALSE
	}

	glEnable(GL_TEXTURE_2D);					// Enable Texture Mapping
	glShadeModel(GL_SMOOTH);					// Enable Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);		// Black Background
	glClearDepth(1.0f);							// Depth Buffer Setup
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);			// Set The Blending Function For Translucency
	glEnable(GL_BLEND);							// Enable Blending

	srand((unsigned)time(0));

	for (count=0; count < floor((GLfloat)num / 2.0f); count++)				// Create A Loop That Goes Through All The Stars
		createStars(false);

	for (count=(GLuint)floor((GLfloat)num / 2.0f); count < num; count++)				// Create A Loop That Goes Through All The Stars
		createStars(true);

	QueryPerformanceFrequency(&performanceFrequency);
	QueryPerformanceCounter(&oldTime);

	return true;								// Initialization Went OK
}

int DrawGLScene(GLvoid)							// Here's Where We Do All The Drawing
{
	QueryPerformanceCounter(&newTime);

	LARGE_INTEGER diff;
	diff.QuadPart = newTime.QuadPart - oldTime.QuadPart;
	frameDelta = (((GLfloat)diff.QuadPart) / ((GLfloat)performanceFrequency.QuadPart));
	logicUpdateCounter += frameDelta;
	
	while(logicUpdateCounter >= logicFpsStep)
	{
		logicUpdateCounter -= logicFpsStep;

		angle += angleSpeed * logicFpsStep;// / 1000000;
		for (count=0; count<num; count++)				// Loop Through All The Stars
		{
			star[count].position.z += speed * logicFpsStep;

			if(star[count].position.z > 0.0f)
			{
				if(count < floor((GLfloat)num / 2.0f))
					createStars(false);
				else
					createStars(true);
			}
		}
	}

	//render
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen And The Depth Buffer
	glBindTexture(GL_TEXTURE_2D, texture[0]);	// Select Our Texture

	for (count=0; count<num; count++)				// Loop Through All The Stars
	{
		glLoadIdentity();						// Reset The View Before We Draw Each Star
		glRotatef(angle, 0.0f, 0.0f, 1.0f);

		glTranslatef(	star[count].position.x,
						star[count].position.y,
						star[count].position.z);

		// Assign A Color Using Bytes
		glColor4ub(star[count].r, star[count].g, star[count].b, 255);
		glBegin(GL_QUADS);						// Begin Drawing The Textured Quad
			glTexCoord2f(0.0f, 0.0f); glVertex3f(-0.1f,-0.1f, 0.0f);
			glTexCoord2f(1.0f, 0.0f); glVertex3f( 0.1f,-0.1f, 0.0f);
			glTexCoord2f(1.0f, 1.0f); glVertex3f( 0.1f, 0.1f, 0.0f);
			glTexCoord2f(0.0f, 1.0f); glVertex3f(-0.1f, 0.1f, 0.0f);
		glEnd();
	}

	oldTime = newTime;

	return true;								// Everything Went OK
}

GLvoid KillGLWindow(GLvoid)						// Properly Kill The Window
{
	if (fullscreen)								// Are We In Fullscreen Mode?
	{
		ChangeDisplaySettings(NULL,0);			// If So Switch Back To The Desktop
		ShowCursor(true);						// Show Mouse Pointer
	}

	if (hRC)									// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))			// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL, "Release Of DC And RC Failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))				// Are We Able To Delete The RC?
		{
			MessageBox(NULL, "Release Rendering Context Failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		}

		hRC = NULL;								// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd, hDC))			// Are We Able To Release The DC
	{
		MessageBox(NULL, "Release Device Context Failed.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hDC = NULL;								// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))			// Are We Able To Destroy The Window?
	{
		MessageBox(NULL, "Could Not Release hWnd.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hWnd = NULL;							// Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL", hInstance))	// Are We Able To Unregister Class
	{
		MessageBox(NULL, "Could Not Unregister Class.", "SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hInstance = NULL;						// Set hInstance To NULL
	}
}

bool CreateGLWindow(char* title, int width, int height, int bits, bool fullscreenflag)
{
	GLuint PixelFormat;							// Holds The Results After Searching For A Match
	WNDCLASS wc;								// Windows Class Structure
	DWORD dwExStyle;							// Window Extended Style
	DWORD dwStyle;								// Window Style

	RECT WindowRect;							// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left = (long)0;					// Set Left Value To 0
	WindowRect.right = (long)width;				// Set Right Value To Requested Width
	WindowRect.top = (long)0;					// Set Top Value To 0
	WindowRect.bottom = (long)height;			// Set Bottom Value To Requested Height

	fullscreen = fullscreenflag;				// Set The Global Fullscreen Flag

	hInstance = GetModuleHandle(NULL);			// Grab An Instance For Our Window
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Move, And Own DC For Window
	wc.lpfnWndProc = (WNDPROC) WndProc;			// WndProc Handles Messages
	wc.cbClsExtra = 0;							// No Extra Window Data
	wc.cbWndExtra = 0;							// No Extra Window Data
	wc.hInstance = hInstance;					// Set The Instance
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);		// Load The Default Icon
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);	// Load The Arrow Pointer
	wc.hbrBackground = NULL;					// No Background Required For GL
	wc.lpszMenuName = NULL;						// We Don't Want A Menu
	wc.lpszClassName = "OpenGL";				// Set The Class Name

	if (!RegisterClass(&wc))					// Attempt To Register The Window Class
	{
		MessageBox(NULL, "Failed To Register The Window Class.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;							// Exit And Return FALSE
	}

	if (fullscreen)								// Attempt Fullscreen Mode?
	{
		DEVMODE dmScreenSettings;				// Device Mode
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));	// Makes Sure Memory's Cleared
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);		// Size Of The Devmode Structure
		dmScreenSettings.dmPelsWidth = width;	// Selected Screen Width
		dmScreenSettings.dmPelsHeight = height;	// Selected Screen Height
		dmScreenSettings.dmBitsPerPel = bits;	// Selected Bits Per Pixel
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Try To Set Selected Mode And Get Results.  NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
		if (ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
		{
			// If The Mode Fails, Offer Two Options.  Quit Or Run In A Window.
			if (MessageBox(NULL, "The Requested Fullscreen Mode Is Not Supported By\nYour Video Card. Use Windowed Mode Instead?", "NeHe GL", MB_YESNO | MB_ICONEXCLAMATION) == IDYES)
			{
				fullscreen = false;				// Select Windowed Mode (Fullscreen = false)
			}
			else
			{
				// Pop Up A Message Box Letting User Know The Program Is Closing.
				MessageBox(NULL, "Program Will Now Close.", "ERROR" ,MB_OK | MB_ICONSTOP);
				return false;					// Exit And Return FALSE
			}
		}
	}

	if (fullscreen)								// Are We Still In Fullscreen Mode?
	{
		dwExStyle = WS_EX_APPWINDOW;			// Window Extended Style
		dwStyle = WS_POPUP;						// Windows Style
		ShowCursor(false);						// Hide Mouse Pointer
	}
	else
	{
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;	// Window Extended Style
		dwStyle = WS_OVERLAPPEDWINDOW;			// Windows Style
	}

	AdjustWindowRectEx(&WindowRect, dwStyle, false, dwExStyle);	// Adjust Window To True Requested Size

	if (!(hWnd=CreateWindowEx(dwExStyle,		// Extended Style For The Window
					"OpenGL",					// Class Name (Must Be Same As Registered Window Class Name)
					title,						// Window Title
					WS_CLIPSIBLINGS |			// Required Window Style
					WS_CLIPCHILDREN |			// Required Window Style
					dwStyle,					// Selected Window Style
					0, 0,						// Window Position
					WindowRect.right - WindowRect.left,	// Calculate Adjusted Window Width
					WindowRect.bottom - WindowRect.top,	// Calculate Adjusted Window Height
					NULL,						// No Parent Window
					NULL,						// No Menu
					hInstance,					// Instance
					NULL)))						// Don't Pass Anything To WM_CREATE
	{
		KillGLWindow();							// Reset The Display
		MessageBox(NULL, "Window Creation Error.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;							// Return FALSE
	}

	static PIXELFORMATDESCRIPTOR pfd =			// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),			// Size Of This Pixel Format Descriptor
		1,										// Version Number
		PFD_DRAW_TO_WINDOW |					// Format Must Support Window
		PFD_SUPPORT_OPENGL |					// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,						// Must Support Double Buffering
		PFD_TYPE_RGBA,							// Request An RGBA Format
		bits,									// Select Our Color Depth
		0, 0, 0, 0, 0, 0,						// Color Bits Ignored
		0,										// No Alpha Buffer
		0,										// Shift Bit Ignored
		0,										// No Accumulation Buffer
		0, 0, 0, 0,								// Accumulation Bits Ignored
		16,										// 16Bit Z-Buffer (Depth Buffer)
		0,										// No Stencil Buffer
		0,										// No Auxiliary Buffer
		PFD_MAIN_PLANE,							// Main Drawing Layer
		0,										// Reserved
		0, 0, 0									// Layer Masks Ignored
	};

	if (!(hDC = GetDC(hWnd)))					// Did We Get A Device Context?
	{
		KillGLWindow();							// Reset The Display
		MessageBox(NULL, "Can't Create A GL Device Context.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;							// Return FALSE
	}

	if (!(PixelFormat = ChoosePixelFormat(hDC, &pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow();							// Reset The Display
		MessageBox(NULL, "Can't Find A Suitable PixelFormat.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;							// Return FALSE
	}

	if(!SetPixelFormat(hDC, PixelFormat, &pfd))	// Are We Able To Set The Pixel Format?
	{
		KillGLWindow();							// Reset The Display
		MessageBox(NULL, "Can't Set The PixelFormat.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;							// Return FALSE
	}

	if (!(hRC = wglCreateContext(hDC)))			// Are We Able To Get A Rendering Context?
	{
		KillGLWindow();							// Reset The Display
		MessageBox(NULL, "Can't Create A GL Rendering Context.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;							// Return FALSE
	}

	if(!wglMakeCurrent(hDC, hRC))				// Try To Activate The Rendering Context
	{
		KillGLWindow();							// Reset The Display
		MessageBox(NULL, "Can't Activate The GL Rendering Context.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;							// Return FALSE
	}

	ShowWindow(hWnd, SW_SHOW);					// Show The Window
	SetForegroundWindow(hWnd);					// Slightly Higher Priority
	SetFocus(hWnd);								// Sets Keyboard Focus To The Window
	ReSizeGLScene(width, height);				// Set Up Our Perspective GL Screen

	if (!InitGL())								// Initialize Our Newly Created GL Window
	{
		KillGLWindow();							// Reset The Display
		MessageBox(NULL, "Initialization Failed.", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;							// Return FALSE
	}

	return true;								// Success
}

LRESULT CALLBACK WndProc(HWND hWnd,				// Handle For This Window
				UINT uMsg,						// Message For This Window
				WPARAM wParam,					// Additional Message Information
				LPARAM lParam)					// Additional Message Information
{
	switch (uMsg)								// Check For Windows Messages
	{
		case WM_ACTIVATE:						// Watch For Window Activate Message
		{
			if (!HIWORD(wParam))				// Check Minimization State
			{
				active = true;					// Program Is Active
			}
			else
			{
				active = false;					// Program Is No Longer Active
			}

			return 0;							// Return To The Message Loop
		}
		case WM_SYSCOMMAND:						// Intercept System Commands
		{
			switch (wParam)						// Check System Calls
			{
				case SC_SCREENSAVE:				// Screensaver Trying To Start?
				case SC_MONITORPOWER:			// Monitor Trying To Enter Powersave?
				return 0;						// Prevent From Happening
			}
			break;								// Exit
		}
		case WM_CLOSE:							// Did We Receive A Close Message?
		{
			PostQuitMessage(0);					// Send A Quit Message
			return 0;							// Jump Back
		}
		case WM_KEYDOWN:						// Is A Key Being Held Down?
		{
			keys[wParam] = true;				// If So, Mark It As TRUE
			return 0;							// Jump Back
		}
		case WM_KEYUP:							// Has A Key Been Released?
		{
			keys[wParam] = false;				// If So, Mark It As FALSE
			return 0;							// Jump Back
		}
		case WM_SIZE:							// Resize The OpenGL Window
		{
			ReSizeGLScene(LOWORD(lParam), HIWORD(lParam));	// LoWord=Width, HiWord=Height
			return 0;							// Jump Back
		}
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE hInstance,			// Instance
			HINSTANCE hPrevInstance,			// Previous Instance
			LPSTR lpCmdLine,					// Command Line Parameters
			int nCmdShow)						// Window Show State
{
	MSG	msg;									// Windows Message Structure
	bool done = false;							// Bool Variable To Exit Loop

	// Ask The User Which Screen Mode They Prefer
	if (MessageBox(NULL, "Would You Like To Run In Fullscreen Mode?", "Start FullScreen?", MB_YESNO | MB_ICONQUESTION) == IDNO)
	{
		fullscreen = false;						// Windowed Mode
	}

	// Create Our OpenGL Window
	if (!CreateGLWindow("Starfield", 640, 480, 16, fullscreen))
	{
		return 0;								// Quit If Window Was Not Created
	}

	while(!done)								// Loop That Runs Until done = TRUE
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message == WM_QUIT)			// Have We Received A Quit Message?
			{
				done = true;					// If So done=TRUE
			}
			else								// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);			// Translate The Message
				DispatchMessage(&msg);			// Dispatch The Message
			}
		}
		else									// If There Are No Messages
		{
			// Draw The Scene.  Watch For ESC Key And Quit Messages From DrawGLScene()
			if (active)							// Program Active?
			{
				if (keys[VK_ESCAPE])			// Was ESC Pressed?
				{
					done = true;				// ESC Signalled A Quit
				}
				else							// Not Time To Quit, Update Screen
				{
					DrawGLScene();				// Draw The Scene
					SwapBuffers(hDC);			// Swap Buffers (Double Buffering)

					if (keys['R'])				// Has The R Key Been Pressed
					{
						angleSpeed = 0.0f;      // Reset AngleSpeed
					}

					if (keys['S'])				// Has The S Key Been Pressed
					{
						speed = DEFAULT_SPEED;          // Reset Speed
					}

					if (keys['C'] && !vkCPressed)
					{
						vkCPressed = true;
						showColors = !showColors;
					}

					if (!keys['C'])
					{
						vkCPressed = false;
					}

					if (keys[VK_UP] && !vkUPPressed)			// Is Up Arrow Being Pressed
					{
						vkUPPressed = true;
						//radius -= 0.01f;			// decrease radius size
						if(radius - RADIUS_INC >= MIN_RADIUS)
							radius -= RADIUS_INC; // decrease radius size
					}

					if (!keys[VK_UP])			// Is Up Arrow Being Pressed
					{
						vkUPPressed = false;
					}

					if (keys[VK_DOWN] && !vkDownPressed)			// Is Down Arrow Being Pressed
					{
						vkDownPressed = true;
						//radius += 0.01f;			// Tilt The Screen Down
						if(radius + RADIUS_INC <= MAX_RADIUS)
							radius += RADIUS_INC;
					}

					if (!keys[VK_DOWN])			// Is Down Arrow Being Pressed
					{
						vkDownPressed = false;
					}

					if (keys[VK_RIGHT] && !vkRightPressed)			// Is Up Arrow Being Pressed
					{
						vkRightPressed = true;
						//angleSpeed -= 1.0f;			// decrease radius size
						if(angleSpeed - ROT_INC >= MIN_ROT)
							angleSpeed -= (GLfloat)ROT_INC; // decrease radius size
					}

					if (!keys[VK_RIGHT])			// Is Up Arrow Being Pressed
					{
						vkRightPressed = false;
					}

					if (keys[VK_LEFT] && !vkLeftPressed)			// Is Up Arrow Being Pressed
					{
						vkLeftPressed = true;
						//angleSpeed += 1.0f;			// decrease radius size
						if(angleSpeed + ROT_INC <= MAX_ROT)
							angleSpeed += (GLfloat)ROT_INC; // decrease radius size
					}

					if (!keys[VK_LEFT])			// Is Up Arrow Being Pressed
					{
						vkLeftPressed = false;
					}

					if (keys[VK_PRIOR] && !vkPriorPressed)			// Is Up Arrow Being Pressed
					{
						vkPriorPressed = true;
						//speed += 0.0001f;			// decrease radius size
						if(speed + SPEED_INC <= MAX_SPEED)
							speed += (GLfloat)SPEED_INC; // decrease radius size
					}

					if (!keys[VK_PRIOR])			// Is Up Arrow Being Pressed
					{
						vkPriorPressed = false;
					}

					if (keys[VK_NEXT] && !vkNextPressed)			// Is Up Arrow Being Pressed
					{
						vkNextPressed = true;
						//speed -= 0.0001f;		// decrease radius size
						if(speed - SPEED_INC >= MIN_SPEED)
							speed -= (GLfloat)SPEED_INC; // decrease radius size
					}

					if (!keys[VK_NEXT])			// Is Up Arrow Being Pressed
					{
						vkNextPressed = false;
					}

					if (keys['1'])				// Is One Being Pressed
					{
						num = (int)(MAX_STARS * 0.1);
					}

					if (keys['2'])				// Is Two Being Pressed
					{
						num = (int)(MAX_STARS * 0.2);
					}

					if (keys['3'])				// Is Three Being Pressed
					{
						num = (int)(MAX_STARS * 0.3);
					}

					if (keys['4'])				// Is Four Being Pressed
					{
						num = (int)(MAX_STARS * 0.4);
					}

					if (keys['5'])				// Is Five Being Pressed
					{
						num = (int)(MAX_STARS * 0.5);
					}

					if (keys['6'])				// Is Six Being Pressed
					{
						num = (int)(MAX_STARS * 0.6);
					}

					if (keys['7'])				// Is Seven Being Pressed
					{
						num = (int)(MAX_STARS * 0.7);
					}

					if (keys['8'])				// Is Eight Being Pressed
					{
						num = (int)(MAX_STARS * 0.8);
					}

					if (keys['9'])				// Is Nine Being Pressed
					{
						num = (int)(MAX_STARS * 0.9);
					}

					if (keys['0'])				// Is Ten Being Pressed
					{
						num = (int)MAX_STARS;
					}
				}
			}
			
			if (keys[VK_F1])					// Is F1 Being Pressed?
			{
				keys[VK_F1] = false;			// If So Make Key FALSE
				KillGLWindow();					// Kill Our Current Window
				fullscreen = !fullscreen;		// Toggle Fullscreen / Windowed Mode
				
				// Recreate Our OpenGL Window
				if (!CreateGLWindow("Starfield", 640, 480, 16, fullscreen))
				{
					return 0;					// Quit If Window Was Not Created
				}
			}
		}
	}

	// Shutdown
	KillGLWindow();								// Kill The Window
	return (msg.wParam);						// Exit The Program
}