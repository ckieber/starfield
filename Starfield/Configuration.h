#ifndef __Configuration__
#define __Configuration__

#include <windows.h.>
#include <gl\gl.h>
#include <string>

class Configuration
{
public:
	Configuration() { };
	Configuration(const int numStars,
				  const GLfloat radius,
				  const GLfloat rotation,
				  const GLfloat speed,
				  const GLfloat time);
	~Configuration();

	//set methods
	void setNumStars(const int numStars);
	void setShowColors(const std::string showColors);
	void setShowColors(const bool showColors);
	void setRadius(const GLfloat radius);
	void setRotation(const GLfloat rotation);
	void setSpeed(const GLfloat speed);
	void setTime(const GLfloat time);

	//get methods
	int getNumStars() const;
	bool showColors() const;
	GLfloat getRadius() const;
	GLfloat getRotation() const;
	GLfloat getSpeed() const;
	GLfloat getTime() const;

private:
	int mNumStars;
	bool mShowColors;
	GLfloat mRadius;
	GLfloat mRotation;
	GLfloat mSpeed;
	GLfloat mTime;
};

#endif